package com.mumo.appconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText editMillimeters = findViewById(R.id.ed_millimeters);
        final EditText editInches = findViewById(R.id.ed_inches);
        editInches.setEnabled(false);
        Button searchButton = findViewById(R.id.btn_search);
        Button exitButton = findViewById(R.id.btn_exit);


        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editInches.setText("");
                String millimeters = editMillimeters.getText().toString();
                if(TextUtils.isEmpty(millimeters)){
                    return;
                }

                try {
                    int mills = Integer.parseInt(millimeters);
                    double inches = mills / 25.4;

                    editInches.setText(String.valueOf(inches));
                }catch (NumberFormatException e){
                    editInches.setText("Invalid input");
                }

            }
        });
    }
}
